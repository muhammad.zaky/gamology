import LoginReducer from "./LoginReducer";
import ScoreReducer from "./ScoreReducer";
import { combineReducers } from "redux";

const Reducers = combineReducers({
    login: LoginReducer,
    score: ScoreReducer
})

export default Reducers;