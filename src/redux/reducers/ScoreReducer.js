import axios from "axios";

const initialData = {
    player: 0,
    computer: 0
}

const ScoreReducer = (state = initialData, action) => {
    switch (action.type) {
        case "SCOREPLAYER":
            const currentPlayerScore = state.player + 1;
            return {
                ...state, 
                player: currentPlayerScore
            }
        case "SCORECOM":
            return {
                ...state,
                computer: state.computer + 1
            }
        default:
            return state
    }
}

export default ScoreReducer;