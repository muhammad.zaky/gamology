let username = localStorage.getItem("username");
if(!username) username = false

const LoginReducer = (state = username, action) => {
    console.log("ini reducer")
    switch (action.type) {
        case "LOGIN":
            return action.payload;
        case "LOGOUT":
            return false;
        default:
            return state;
    }
}

export default LoginReducer;