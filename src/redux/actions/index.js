export const loginAction = (username) => {
    return {
        type: "LOGIN",
        payload: username
    }
}

export const playerMenang = () => {
    return {
        type: "SCOREPLAYER"
    }
}

export const comMenang = () => {
    return {
        type: "SCORECOM"
    }
}