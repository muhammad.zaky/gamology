import React, { useEffect } from "react";
import { Button } from "./Button";
import { Link, useHistory } from "react-router-dom";
import { useSelector } from "react-redux";
import "./Navbar.css";

function NavbarUser() {
  const login = useSelector(state => state.login)

  const history = useHistory();

  useEffect(() => {
    console.log("halaman di reload")
    if(!login){
      history.push("/login");
    }
  }, [])

  const handleLogout = () => {
    localStorage.removeItem("token");
    localStorage.removeItem("username");
    history.push("/login");
  };
  return (
    <>
      <nav className="active navuser">
        <Link to="/" className="navbar-logo">
          GOJEK
          <i class="fab fa-typo3" />
        </Link>
        <div className="menu-icon">
          <i className="fas fa-times" />
        </div>
        <ul className="nav-menu">
          <li className="nav-item">
            <Link to="/profile" className="nav-links">
              Profile
            </Link>
          </li>
          <li className="nav-item">
            <Link to="/listgame" className="nav-links">
              List Game
            </Link>
          </li>
        </ul>
        <Button buttonStyle="btn--normal" onClick={() => handleLogout()}>
          LOG OUT
        </Button>
        <Button buttonStyle="btn--normal disable">
          {login}
        </Button>
      </nav>
    </>
  );
}

export default NavbarUser;
