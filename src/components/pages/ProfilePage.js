import React from "react";
import Profile from "../Profile";
import NavbarUser from "../NavbarUser";

export default function RegisterPage() {
  return (
    <>
      <NavbarUser />
      <Profile></Profile>
    </>
  );
}
