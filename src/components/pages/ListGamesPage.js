import React from "react";
import ListGame from "../ListGame";
import NavbarUser from "../NavbarUser";
import { useSelector } from "react-redux";

export default function RegisterPage() {
  const login = useSelector(state => state.login);
  
  
  return (
    <>
      <NavbarUser></NavbarUser>
      <ListGame></ListGame>
    </>
  );
}
