import React from "react";
import "../../App.css";

import Home from "../Home";
import TheGame from "../TheGame";
import Features from "../Features";
import Requirement from "../Requirement";
import TopScore from "../TopScore";
import Footer from "../Footer";

function LandingPage() {
  return (
    <>
      <Home />
      <TheGame></TheGame>
      <Features></Features>
      <Requirement></Requirement>
      <TopScore></TopScore>
      <Footer />
    </>
  );
}

export default LandingPage;
