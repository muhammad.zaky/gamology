import React from "react";
import "../../App.css";
import { useSelector } from "react-redux";

export default function About() {
  const username = useSelector(state => state.login);
  return <h1 className="about">{username.toUpperCase()}</h1>;
}
