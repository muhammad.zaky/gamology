import React from "react";
import "./Features.css";

function Features() {
  return (
    <div className="features-container">
      <div className="flex-container">
        <section className="features"></section>
        <section className="features">
          <p className="features-text">What's so special?</p>
          <p className="features-heading">FEATURES</p>
          <div class="features-item">
            <h4>TRADITIONAL GAMES</h4>
            <h5>
              If you miss your childhood, we provide many traditional game here{" "}
            </h5>
          </div>
          <div class="features-item">
            <h4>Game Suit</h4>
          </div>
          <div class="features-item">
            <h4>TBD</h4>
          </div>
        </section>
      </div>
    </div>
  );
}

export default Features;
