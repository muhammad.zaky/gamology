import React from "react";
import "../App.css";
import { Button } from "./Button";
import "./Home.css";
import { Link } from "react-router-dom";
import { animateScroll as scroll } from "react-scroll";

function Home() {
  const scrolling = () => scroll.scrollToBottom();
  return (
    <div className="hero-container">
      <h1>PLAY TRADISIONAL GAME</h1>
      <p>Experience new tradisional game play</p>
      <Link to="/suwit" className="hero-btns">
        <Button
          className="btns"
          buttonStyle="btn--primary"
          buttonSize="btn--large"
          onClick={console.log("hey")}
        >
          Play Now <i className="far fa-play-circle" />
        </Button>
      </Link>
      <h2 className="bot-center2">
        <i>The Story</i>
      </h2>
      <Link to="/" className="btn-scroll bot-center" onClick={scrolling}>
        <i className="fas fa-chevron-down"></i>
      </Link>
    </div>
  );
}

export default Home;
