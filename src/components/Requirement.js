import React from "react";
import "./Requirement.css";

function Requirement() {
  return (
    <div className="requirement-container">
      <div className="flex-container">
        <section className="requirement">
          <p className="requirement-text">Can My Computer Run This Game?</p>
          <p className="requirement-heading">SYSTEM REQUIREMENTS</p>
          <table class="table-bordered">
            <tbody>
              <tr>
                <td>
                  <h3>OS:</h3>
                  <h4>Windows 7 64-bit only</h4>
                  <h4>(No OSX support at this time)</h4>
                  <br />
                </td>
                <td>
                  <h3>PROCESSOR:</h3>
                  <h4>Intel Core 2 Duo @ 2.4 GHZ or</h4>
                  <h4>AMD Athlon X2 @ 2.8 GHZ</h4>
                  <br />
                </td>
              </tr>
              <tr>
                <td>
                  <h3>MEMORY:</h3>
                  <h4>4 GB RAM</h4>
                  <br />
                </td>
                <td>
                  <h3>STORAGE:</h3>
                  <h4>8 GB available space</h4>
                  <br />
                  <br />
                </td>
              </tr>
              <tr>
                <td colspan="2">
                  <h3>GRAPHICS:</h3>
                  <h4>NVIDIA GeForce GTX 660 2GB or</h4>
                  <h4>AMD Radeon HD 7850 2GB DirectX11 (Shader Model 5)</h4>
                  <br />
                </td>
              </tr>
            </tbody>
          </table>
        </section>
        <section className="requirement"></section>
      </div>
    </div>
  );
}

export default Requirement;
