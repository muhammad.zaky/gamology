import React from "react";
import "./TheGame.css";
import ImageSlider from "./ImageSlider";
import { SliderData } from "./SliderData";

function TheGame() {
  return (
    <div className="thegame-container">
      <div className="flex-container">
        <section className="thegame">
          <p className="thegame-text">What so special's?</p>
          <p className="thegame-heading">THE GAMES</p>
        </section>
        <section className="thegame">
          <ImageSlider slides={SliderData} />
        </section>
      </div>
    </div>
  );
}

export default TheGame;
