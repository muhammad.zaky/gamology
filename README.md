Membuat Redux

1. Buat Folder redux
2. Di dalam folder, buat 2 folder. reducers dan actions
3. Buat reducer 
contoh reducer
```
const LoginReducer = (state = false, action) => {
    switch (action.type) {
        case "LOGIN":
            return true;
        case "LOGOUT":
            return false;
        default:
            return state;
    }
}
```

4. Create store di element tertinggi
contoh create store
```
import { createStore } from "redux";
const store = createStore(REDUCER)
```

5. Create Provide
contoh provider
```
import { Provider } from "react-redux";
<Provider store={store}>disini component kita</Provider>
```

6. import all reducer
6.1 buat index di folder reducers
contoh
```
import LoginReducer from "./LoginReducer";
import { combineReducers } from "redux";
const Reducers = combineReducers({
    login: LoginReducer
})
```

`export default Reducers;`


6.2 panggil all reducer
`import Reducers from "./redux/reducers"`

7. menggunakan data dari store dengan useSelector.

8. update data di store dengan menggunakan action melalui dispatch.
8.1 tambahkan fungsi update data di action
8.2 memanggil fungsi update data menggunakan dispatch